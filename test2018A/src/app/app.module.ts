import { UsersService } from './users/users.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import {RouterModule} from '@angular/router';


//FireBase
import { environment } from './../environments/environment';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';

import { AppComponent } from './app.component';
import { NavigationComponent } from './navigation/navigation.component';
import { UsersComponent } from './users/users.component';

import { LoginComponent } from './login/login.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { FusersComponent } from "./users/fusers/fusers.component";


@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    UsersComponent,

    LoginComponent,
    NotFoundComponent,
    FusersComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
     AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    RouterModule.forRoot([

      {path:'login', component:LoginComponent}, // localhost:4200
      {path:'products from firebase', component:FusersComponent},
     {path: '', component: UsersComponent}, //default - localhost:4200 - homepage
      {path:'**', component:NotFoundComponent}, 
    ])
  ],
  providers: [UsersService],
  bootstrap: [AppComponent]
})
export class AppModule { }