import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FusersComponent } from './fusers.component';

describe('FusersComponent', () => {
  let component: FusersComponent;
  let fixture: ComponentFixture<FusersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FusersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FusersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
