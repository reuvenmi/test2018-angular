import { Component, OnInit } from '@angular/core';
import { UsersService } from "../users.service";

@Component({
  selector: 'fusers',
  templateUrl: './fusers.component.html',
  styleUrls: ['./fusers.component.css']
})
export class FusersComponent implements OnInit {

  users;
  constructor(private service:UsersService) { }

  ngOnInit() {
    this.service.getUsersFire().subscribe(response =>{
      
      console.log(response);
      this.users = response;
  });

}

}



