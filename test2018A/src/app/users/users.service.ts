import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { HttpParams, HttpHeaders } from '@angular/common/http';
import { Headers } from '@angular/http';
import { AngularFireDatabase } from 'angularfire2/database';
import { environment } from './../../environments/environment';
import 'rxjs/Rx';


@Injectable()
export class UsersService {
  http:Http;


  getUsers(){
    //get users from the SLIM rest API (Don't say DB)
return this.http.get(environment.url + 'users');

   //return this.http.get("http://localhost/angular/slim/users");
   
  }

  getUsersFire(){
    return this.db.list('/users').valueChanges();    
  }
  
 constructor(http:Http, private db:AngularFireDatabase) {
    this.http = http;
   }
}