// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  url:"http://localhost/angular/slim/",
  production: false,
  firebase:{
  apiKey: "AIzaSyCr3S3qXObJyY9vR-l4a8ZFPkJbqGqbAys",
    authDomain: "test2018a-f70e9.firebaseapp.com",
    databaseURL: "https://test2018a-f70e9.firebaseio.com",
    projectId: "test2018a-f70e9",
    storageBucket: "test2018a-f70e9.appspot.com",
    messagingSenderId: "476086278774"
  }
};
  